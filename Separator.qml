import QtQuick

Rectangle {
    id: root
    implicitHeight: 1
    implicitWidth: 1

    enum Weight {
        Light,
        Normal
    }

    property int weight: Separator.Weight.Normal

    color: weight === Separator.Weight.Light ? "red" : "blue"
}
