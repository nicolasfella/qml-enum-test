import QtQuick
import org.kde.mymodule as MyModule

Window {

    width: 500
    height: 500

    visible: true

    Item {
        anchors.fill: parent

        MyModule.Separator {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.topMargin: 10
        }
    }
}
