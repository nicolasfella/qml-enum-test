#ifndef KIRIGAMIPLUGIN_H
#define KIRIGAMIPLUGIN_H

#include <QQmlEngine>
#include <QQmlExtensionPlugin>
#include <QUrl>

class KirigamiPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    KirigamiPlugin(QObject *parent = nullptr);
    void registerTypes(const char *uri) override;

private:
    QUrl componentUrl(const QString &fileName) const;
};

#endif
