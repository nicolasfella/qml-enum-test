#include <QQmlApplicationEngine>
#include <QGuiApplication>
#include <QDir>

int main(int argc, char**argv){

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    engine.load("qrc:/septest.qml");

    return app.exec();
}
