#include "plugin.h"

#include <QQmlContext>
#include <QQuickItem>


KirigamiPlugin::KirigamiPlugin(QObject *parent)
    : QQmlExtensionPlugin(parent)
{
}

void KirigamiPlugin::registerTypes(const char *uri)
{
    qmlRegisterType(QUrl(baseUrl().toString() + "/" + QStringLiteral("Separator.qml")), uri, 2, 0, "Separator");
}


#include "plugin.moc"
